# Contributing to lux

## Code

Contributions of code are accepted through [GitLab's merge requests][merge-requests].

## Bugs

Bugs should be reported to [GitLab's issue tracker][issues]. It is recommended
that you read Simon Tatham's excellent article,
["How to Report Bugs Effectively"][reporting-bugs].

[reporting-bugs]:   http://www.chiark.greenend.org.uk/~sgtatham/bugs.html
[merge-requests]:   https://gitlab.com/somasis/lux/merge_requests
[issues]:           https://gitlab.com/somasis/lux/issues
